[[!meta title="Cooking with Cairo"]]

# Scrumptious recipes to spice up your applications

+ General Drawing
    * [[Rounded_rectangles|roundedrectangles]]
    * [[Ellipses With Uniform Stroke Width|ellipses]]
    * [[A_description_of_compositing_operators_in_Cairo|operators]]
    * [[Clipping Outside A Region|outside_clipping]]

+ Matrix Transformations
    * [[Basic_matrix_transformation_reminder|matrix_transform]]
    * [[Keeping Your Matrix Transformations Straight|matrix_conventions]]
    * [[Transforming About An Arbitrary Point|transform_about_point]]

+ Fonts and Text
    * [[Loading_fonts_using_FreeType_for_cairo_use_in_Python|freetypepython]]
    * [[How_to:Render text with Pango using Python|pycairo_pango]]

+ Graphics Formats
    * [[Load_jpg/png/gif/bmp/etc_into_CairoContext_with_help_from_gdk-pixbuf_in_Pycairo/Pygtk|gdkpixbufpycairo]]
    * [[Python:_Converting_PIL_images_to_Cairo_surfaces_and_back|pythoncairopil]]
    * [[Rendering_SVG_using_librsvg_in_Python|librsvgpython]]
    * [[How_to:Open_a_simple_SVG_file_without_Pyrsvg|svgtopycairo]]
    * [[How_to:Using_PyCairo_and_Pyrsvg_to_handle_SVG_graphics|pyrsvg]]
    * [[How_to:Render_PDF_and_PostScript_files_with_cairo|renderpdf]]

+ Interactive Graphics
    * [[How_to:Using_path_data_to_create_a_Hit_Area|hittestpython]]
    * [[How_to:Do_timeout_animation_(Also_shows_rotation_around_a_point)|animationRotation]]

+ Image Processing
    * [[How_to:Perform a Gaussian blur on an image surface|blur.c]]
    * [[How_to:Apply an emboss filter to an image surface|emboss]]

+ Language Bindings
    * [[Using_Perl_Cairo_module|perl_cairo_module]]
    * [[Converting_cairo_code_from_C_to_Python_or_Haskell_and_back|ConvertCtoPyAndBack]]
    * [[How_to:A_quick_framework_for_testing_cairo_from_Python_(And_demos_masking)|quickframework]]

+ Platform-specific
    * [[Using_Win32_Quickstart|win32quickstart]]
    * [[.NET GDI rendering|dotnet-gdi-rendering]]
    * [[How_to:Create and use an XcbSurface|xcbsurface.c]]
