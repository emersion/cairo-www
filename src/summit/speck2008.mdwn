[[!meta title="Speck HackFest 2008"]]

The GNOME Foundation has organised a room for Cairo development at the
[Speck HackFest](http://live.gnome.org/SpeckHackFest2008).

Currently planning to attend are:

 * Behdad Esfahbod

 * Benjamin Otte

   Arrival: ~1930

 * Joonas Pihlaja

   Arrival: ~1700

 * Carl Worth

 * Chris Wilson

   Arrival: at Bolzano (BZ0), 9th November, 14:35H
   Departure: from Bolzano (BZO), 15th November, 15:15H

The topic for the gathering is the opening of the 1.9 development cycle and
integration of all the wacky ideas that have been brewing recently. There may
even be time for some performance optimization...

Venue:
Carl will be staying at the Hotel Regina, while the rest of the gang will
(most likely be) in the Ostello della Gioventù di Bolzano - Bozen,
Via Renon 23, 39100 Bolzano (BZ).
