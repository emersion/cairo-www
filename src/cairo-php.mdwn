# PHP Bindings

PECL cairo is the official Cairo Graphics Library binding.  It provides an object oriented and procedural interface for the cairo library.
Support is currently provided for 1.4 to 1.8 versions of the library (1.10 is partially supported).  The extension is available for PHP 5.2.0 and higher only.  The package can be found at <http://pecl.php.net/package/cairo>

The extension has the majority of items wrapped, still missing are some font backends (Quartz and User), and system specific surfaces (win32, Quartz and Xlib)

# Installation

PECL cairo can be installed on linux and unix systems with PEAR installed using the command

pecl install cairo

# Download

PECL cairo is available from PHP subversion.

<http://svn.php.net/pecl/cairo/>

And releases are available from the PECL package page.

# Documentation

<http://php.net/cairo> has partial documentation.  For more information, refer to the [cairo manual](http://cairographics.org/manual/). Most of its content is directly applicable to the PHP extension as well.

# Other Extensions

There are two additional PHP bindings (extensions) available that are superceded by PECL cairo. Neither of them is complete but both should be sufficient to do the basic stuff.

# php-cairo

php-cairo is a completely object orientated cairo binding for PHP 5+ and is available here: <http://sourceforge.net/projects/klecks/>

# cairo-wrapper

cairo-wrapper is a procedural PECL extension and also works under PHP 4. <http://pecl.php.net/package/cairo_wrapper>

Users of the cairo_wrapper extension can use PECL cairo as a drop in replacement if desired.
