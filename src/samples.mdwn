[[!meta title="Cairo samples"]]

This page contains samples of cairo's rendered output and the code
snippets used to create them. The snippets are meant to be short, and
easy to understand.

The original snippets were created by Øyvind Kolås for a paper submitted
to [GUADEC 2004][39]. All of his original snippet code is considered to
be part of the public domain.

[[!inline pages="samples/* and !samples/*.png" show="0" template="cairo_sample" sort="title"]]

   [1]: http://cairographics.org/
  [39]: http://2004.guadec.org/
