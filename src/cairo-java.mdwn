[[!meta title="Java bindings"]]

Java bindings for Cairo are available in the [java-gnome](http://java-gnome.sourceforge.net/) user interface library, with just the sort of API you'd expect:

    surface = new ImageSurface(Format.ARGB32, 200, 200);
    cr = new Context(surface)
    cr.setSource(0.0, 0.0, 0.0);
    cr.moveTo(25.0, 25.0);
    cr.lineTo(50.0, 100.0);
    cr.stroke();

you can use the Cairo coverage in java-gnome for drawing custom widgets in GTK, outputting PDFs, and of course images in bitmap PNG and vector SVG form. The Pango library is used for drawing text.

java-gnome is packaged in [Debian](http://packages.debian.org/sid/libjava-gnome-java) and [Ubuntu](http://packages.ubuntu.com/lucid/libjava-gnome-java) (there's also a [PPA](https://launchpad.net/~java-gnome/+archive/ppa) with the latest release)  as the `libjava-gnome-java` package, and in [Gentoo](http://packages.gentoo.org/package/dev-java/java-gnome) as the `dev-java/java-gnome` package, and in many [other](http://java-gnome.sourceforge.net/4.0/get/) fine Linux distros. Check your TV Guide for details.

JavaDoc API documentation is in the `doc/api/` tree and available online as well. The [`Context`](http://java-gnome.sourceforge.net/4.0/doc/api/org/freedesktop/cairo/Context.html) class in `org.freedesktop.cairo` is a good place to start.

There is [example code](http://java-gnome.sourceforge.net/4.0/doc/examples/START.html#ExampleDrawingInExposeEvent) in the `doc/examples/` tree.

If you want to build java-gnome yourself there's a [`README`](http://java-gnome.sourceforge.net/4.0/README.html) to get you going. You can [browse](http://research.operationaldynamics.com/bzr/java-gnome/mainline/src/bindings/org/freedesktop/cairo/) the source code online or get the code from Bazaar; you'd be best off following the [`HACKING`](http://java-gnome.sourceforge.net/4.0/HACKING.html) instructions if you'd like to checkout the code. Releases are at `ftp.gnome.org`.

Questions can be asked in the `#java-gnome` IRC channel on `irc.gimp.net`, or by mailing the `java-gnome-developer` [mailing list](http://java-gnome.sourceforge.net/4.0/lists/).
